# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: smplayer\n"
"PO-Revision-Date: 2017-09-22 21:36+0000\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan (http://www.transifex.com/rvm/smplayer/language/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

# Installer_No_Admin
msgid ""
"You must be logged in as an administrator when installing this program."
msgstr "Heu d'iniciar la sessió com a administrador quan instal·leu aquest programa."

# Win64_Required
msgid ""
"A 64-bit Windows operating system is required to install this software."
msgstr "Es requereix un sistema operatiu Windows de 64 bits per instal·lar aquest programari."

# WelcomePage_Title
msgid "$(^NameDA) Setup"
msgstr "Preparació $(^NameDA)"

# WelcomePage_Text
msgid ""
"Setup will guide you through the installation of "
"$(^NameDA).[:r:][:n:][:r:][:n:]It is recommended that you close all "
"instances of SMTube before starting setup. This will make it possible to "
"update relevant program files without having to reboot your "
"computer.[:r:][:n:][:r:][:n:]$_CLICK"
msgstr "La preparació us guiarà a través de la instal·lació de $(^NameDA).[:r:][:n:][:r:][:n:]Es recomana que tanqueu totes les instàncies de SMTube abans de començar la preparació. Això farà que sigui possible l'actualització dels de programa pertinents sense haver de reiniciar el vostre ordinador.[:r:][:n:][:r:][:n:]$_CLICK"

# Section_SMTube
msgid "SMTube (required)"
msgstr "SMTube (requerit)"

# DirectoryPage_Text
msgid ""
"$(^DirText)[:r:][:n:][:r:][:n:]SMTube must be installed to a directory "
"containing a ${SMTUBE_INST_ARCH} ${SMTUBE_QT_VER} installation of SMPlayer."
msgstr "$(^DirText)[:r:][:n:][:r:][:n:]SMTube ha de ser instal·lat en un directori que contingui una instal·lació ${SMTUBE_INST_ARCH} ${SMTUBE_QT_VER} de SMPlayer."

# DirectoryIsNotWritable
msgid ""
"The chosen installation directory is not writable and may require restarting"
" the installer with administrator privileges.[:r:][:n:][:r:][:n:]Proceed "
"with installation?"
msgstr "El directori d'instal·lació escollit no és modificable i pot requerir reiniciar el programa d'instal·lació amb privilegis d'administrador.[:r:][:n:][:r:][:n:]Voleu continuar amb la instal·lació?"

# Info_QtArch_Bypassed
msgid "SMPlayer, architecture and Qt version check bypassed by command-line."
msgstr "La comprovació de SMPlayer, de l'arquitectura i de la versió de Qt passades amb la línia d'ordres."

# Info_Portable_Bypassed
msgid "Portable check overriden by command-line, forcing portable."
msgstr "La comprovació de la portabilitat ha estat anul·lada per la línia d'ordres, es força la portabilitat."

# Info_Portable_Detected
msgid "Found portable version of SMPlayer."
msgstr "S'ha trobat una versió portable de SMPlayer."
